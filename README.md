# Bio parser
Python tools to extract sequences from FASTA files (from NCBI for instance). 

# Prerequisites

Install `biopython` and `gitpython`: 
```
pip3 install biopython gitpython
```
or (check your environment)
```
pip install biopython gitpython
```


# Run
For instance: 
```
python fasta_parser.py sequence.fasta
```

# Licence

We use the third party software [biopython](https://github.com/biopython/biopython), licensed under a [BSD like license](https://github.com/biopython/biopython/blob/master/LICENSE.rst)

_Bio parser_ is released under [BSD 3-Clause License](https://opensource.org/license/bsd-3-clause/).
