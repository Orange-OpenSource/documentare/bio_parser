import os
import unittest

import git
from git import Repo

from fasta_parser import parse_and_write_sequences, SequenceStatistics, Significance, VERSION, Args, keep_only_alphanum, Sequence, write_summary


def delete_file(file_path: str) -> None:
    if file_exists(file_path):
        os.remove(file_path)


def file_exists(file_path: str) -> bool:
    return os.path.exists(file_path)


def read_file_content(file_path: str) -> str:
    f = open(file_path, 'r')
    content = f.read()
    f.close()
    return content


def clear_files() -> None:
    delete_file('txt/alpha_sequence.txt')
    delete_file('txt/beta_sequence.txt')
    delete_file('txt/gamma_sequence.txt')
    delete_file('html/alpha_sequence.html')
    delete_file('html/beta_sequence.html')
    delete_file('html/gamma_sequence.html')


class TestParser(unittest.TestCase):

    def setUp(self):
        clear_files()

    def tearDown(self):
        clear_files()

    def test_check_version(self):
        repo: Repo = git.Repo(search_parent_directories=True)
        self.assertEqual(repo.git.describe(), VERSION)

    def test_keep_only_alpha_num(self):
        self.assertEqual(keep_only_alphanum('NC_009776_1_Ignicoccus_hospitalis_KIN4/I__complete_sequence.'), 'NC_009776_1_Ignicoccus_hospitalis_KIN4_I__complete_sequence')

    def test_parse_fasta_file_and_write_sequences_files(self):
        # When
        sequences: tuple[Sequence] = parse_and_write_sequences('./test_example.fasta')

        # Then
        self.assertTrue(file_exists('txt/alpha_sequence.txt'))
        self.assertTrue(file_exists('txt/beta_sequence.txt'))
        self.assertTrue(file_exists('txt/gamma_sequence.txt'))
        self.assertTrue(file_exists('html/alpha_sequence.html'))
        self.assertTrue(file_exists('html/beta_sequence.html'))
        self.assertTrue(file_exists('html/gamma_sequence.html'))
        self.assertEqual(read_file_content('txt/alpha_sequence.txt'), 'ACGTA')
        self.assertEqual(read_file_content('txt/beta_sequence.txt'), 'CGTC')
        self.assertEqual(read_file_content('txt/gamma_sequence.txt'), 'CCGCC')

        self.maxDiff = None
        expected_html = read_file_content('test_expected.html')
        actual_html = read_file_content('html/alpha_sequence.html')

        print(actual_html)
        self.assertEqual(actual_html, expected_html)

    def test_write_summary(self):
        # When
        sequences: tuple[Sequence] = parse_and_write_sequences('./test_example.fasta')

        all_sequences: list[tuple[Sequence]] = []
        all_sequences.append(sequences)
        write_summary(all_sequences)

        # Then
        summary = read_file_content('txt/summary.csv')
        if summary.find('-0.09;alpha\n0;beta\n-0.09;gamma') < 0:
            self.fail(f'summary not expected: {summary}')

    def test_statistics(self):
        # Given
        seq = 'AAGAT_AGATG_AGATG_ATGAA_ATGAA_ATGAA_ATTCA_ATTCA_ATTCA_ATTCA_CAAGA_CAAGA_CAAGA_CAAGA_CAAGA_TTCAA_TTCAA_TTCAA_TTCAA_TTCAA_TTCAA_TTCAA_TTCAA_TTCAA'
        # When
        stats = SequenceStatistics(seq)
        # Then
        self.assertEqual(stats.to_string(), f'\n'
                                f'\tsequence length = 143<br>\n'
                                f'\t<br>\n'
                                f'<br>\n'
                                f'\t1) AAGAT count = 1<br>\n'
                                f'\t2) AGATG count = 2<br>\n'
                                f'\t3) ATGAA count = 3<br>\n'
                                f'\t4) ATTCA count = 4<br>\n'
                                f'\t5) CAAGA count = 5<br>\n'
                                f'\t6) GATGA count = 0<br>\n'
                                f'\t7) TCAAG count = 0<br>\n'
                                f'\t8) TGAAT count = 0<br>\n'
                                f'\t9) TTCAA count = 9<br>\n'
                                f'<br>\n'
                                f'\tall) sum = 24<br>\n'
                                f'<br>\n'
                                f'<br>\n'
                                f'\tSignificance = 20.7	(n=139; A=1.22; std=1.1)<br>\n'
                         )

    def test_significance(self):
        # Nb pentamères possibles=n=N-4  Nb total attendu A=np=nx9/1024  Ecart-type=(npq)1/2  Significativité=(O-A)/ecart-type
        #   1662958              14616               (121)          99.5

        # Given
        sequence_length = 1662958 + 4
        observed_pentamers = 26645

        # When
        s: Significance = SequenceStatistics.compute_significance(sequence_length, observed_pentamers)

        # Then
        self.assertEqual(s.to_string(), 'Significance = 99.94	(n=1662958; A=14615.84; std=120.36)')

    def test_args_file(self):
        # When
        args = Args.parse_args(['program.py', 'test_example.fasta'])

        # Then
        self.assertEqual(False, args.error())
        expected_tuple = tuple(['test_example.fasta'])
        self.assertEqual(expected_tuple, args.fasta_files_path())

    def test_args_directory(self):
        # When
        args = Args.parse_args(['program.py', 'test_dir'])

        # Then
        self.assertEqual(False, args.error())
        self.assertEqual(('test_dir/test_example_1.fasta', 'test_dir/test_example_2.fasta'), args.fasta_files_path())

    def test_args_nothing(self):
        # When
        args = Args.parse_args(['program.py'])

        # Then
        self.assertEqual(True, args.error())
        self.assertEqual('❌ [No Argument] please provide fasta file or directory PATH as first argument', args.error_message())

    def test_args_more_than_one_arg(self):
        # When
        args = Args.parse_args(['program.py', 'file_1.fasta', 'file_2.fasta'])

        # Then
        self.assertEqual(True, args.error())
        self.assertEqual('❌ [Too Many Argument] please provide fasta file or directory PATH as SOLE first argument', args.error_message())

    def test_args_file_not_found(self):
        # When
        args = Args.parse_args(['program.py', 'file_1.fasta'])

        # Then
        self.assertEqual(True, args.error())
        self.assertEqual('❌ [File Or Directory Not Found] \'file_1.fasta\' not found', args.error_message())

    def test_args_empty_directory_with_hidden_file(self):
        # When
        args = Args.parse_args(['program.py', 'test_empty_dir'])

        # Then
        self.assertEqual(True, args.error())
        self.assertEqual('❌ [No Files found] in directory \'test_empty_dir\'', args.error_message())


if __name__ == '__main__':
    unittest.main()
