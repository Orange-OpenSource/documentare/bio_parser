import math
import os
import sys
from datetime import datetime
from itertools import chain
from pathlib import Path

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

VERSION = "1.4.0"
PENTAMERS = tuple(sorted(('ATTCA', 'TTCAA', 'TCAAG', 'CAAGA', 'AAGAT', 'AGATG', 'GATGA', 'ATGAA', 'TGAAT')))


class Significance:
    def __init__(self, nb_possible_pentamers: int, nb_expected_pentamers: float, standard_deviation: float, significance: float):
        self.__nb_possible_pentamers = nb_possible_pentamers
        self.__nb_expected_pentamers = nb_expected_pentamers
        self.__standard_deviation = standard_deviation
        self.__significance = significance

    def to_string(self) -> str:
        if self.__significance == 0:
            return f'failed to compute significance \t(n={self.__nb_possible_pentamers}; A={round(self.__nb_expected_pentamers, 2)}; std={round(self.__standard_deviation, 2)})'
        else:
            return f'Significance = {self.rounded()}\t(n={self.__nb_possible_pentamers}; A={round(self.__nb_expected_pentamers, 2)}; std={round(self.__standard_deviation, 2)})'

    def rounded(self):
        return round(self.__significance, 2)

class SequenceStatistics:
    def __init__(self, sequence: str):
        self.__sequence = sequence
        self.__pentamers_count = {}
        self.__pentamers_count_all = 0
        self.__sequence_length = 0
        self.__significance: Significance
        for pentamer in PENTAMERS:
            self.__pentamers_count[pentamer] = 0
        self.__compute()

    def __compute(self) -> None:
        self.__sequence_length = len(self.__sequence)
        for pentamer in PENTAMERS:
            count = self.__sequence.count(pentamer)
            self.__pentamers_count[pentamer] = count
            self.__pentamers_count_all = self.__pentamers_count_all + count

        self.__significance = self.compute_significance(self.__sequence_length, self.__pentamers_count_all)

    def significance(self) -> Significance:
        return self.__significance

    def to_string(self) -> str:
        head = (
            f'\n\tsequence length = {self.__sequence_length}<br>\n'
            '	<br>\n'
        )
        pentamers_count = ''
        i = 1
        for pentamer in PENTAMERS:
            pentamers_count = pentamers_count + f'<br>\n\t{i}) {pentamer} count = {self.__pentamers_count[pentamer]}'
            i = i + 1

        count_all = '<br>\n' + '<br>\n' + f'\tall) sum = {self.__pentamers_count_all}<br>\n'
        significance = '<br>\n' + '<br>\n' + f'\t{self.__significance.to_string()}<br>\n'
        return head + pentamers_count + count_all + significance

    @staticmethod
    def compute_significance(sequence_length: int, observed_pentamers: int) -> Significance:
        nb_possible_pentamers: int = sequence_length - 4
        p: float = 9 / 1024
        q: float = 1 - p
        nb_expected_pentamers: float = nb_possible_pentamers * p
        standard_deviation: float = math.sqrt(nb_possible_pentamers * p * q)
        s = 0
        if standard_deviation != 0:
            s = (observed_pentamers - nb_expected_pentamers) / standard_deviation
        return Significance(nb_possible_pentamers, nb_expected_pentamers, standard_deviation, s)


class Sequence:
    def __init__(self, description: str, seq, statistics: SequenceStatistics):
        self.__description = description
        self.__seq = seq
        self.__statistics = statistics

    def statistics(self) -> SequenceStatistics:
        return self.__statistics

    def description(self) -> str:
        return self.__description

    def seq(self) -> str:
        return self.__seq.__str__()


def parse(fasta_file_path: str) -> list[SeqRecord]:
    return list(SeqIO.parse(fasta_file_path, 'fasta'))


def write_sequence(desc: str, seq: str):
    Path('txt').mkdir(exist_ok=True)
    f = open(f'txt/{desc}_sequence.txt', 'w')
    f.write(seq)
    f.close()


def highlight(seq: str) -> str:
    highlighted_seq = seq
    colors = ['blue', 'purple', 'pink', 'red', 'orange', 'yellow', 'green', 'teal', 'cyan']
    bg_colors = ['white', 'white', 'black', 'white', 'white', 'black', 'white', 'white', 'black']
    i = 0
    for pentamer in PENTAMERS:
        highlighted_seq = highlighted_seq.replace(pentamer, f'<span style="color: {colors[i]}; background-color: {bg_colors[i]}">{pentamer}</span>')
        i = i + 1
    return highlighted_seq


def write_sequence_html(stats_string: str, desc: str, seq: str):
    Path('html').mkdir(exist_ok=True)
    f = open(f'html/{desc}_sequence.html', 'w')
    f.write((
        f'<!DOCTYPE html>\n'
        f'<html lang="en">\n'
        f'<head>\n'
        f'    <meta charset="utf-8">\n'
        f'    <title>{desc}</title>\n'
        f'</head>\n'
        f'<body>\n'
        # f'    <p style="margin: 2em 0">{header()}</p>\n'
        f'    <p style="margin: 2em 0; font-family: monospace;">{stats_string}</p>\n'
        f'    <div style="margin: 2em 0; inline-size: 1280px; overflow-wrap: break-word; word-break: break-all;">\n'
    ))
    f.write(highlight(seq))
    f.write((
        f'    </div>\n'
        f'</body>\n'
        f'</html>\n'
    ))
    f.close()


def keep_only_alphanum(description: str) -> str:
    replace_special_chars = description.replace('.', '_').replace(',', '_').replace(' ', '_').replace('\\', '_').replace('/', '_').replace(':', '_').removesuffix('_')
    return ''.join(letter for letter in replace_special_chars if letter.isalnum() or letter == '_')


def write_sequences(sequences: tuple[Sequence]):
    for sequence in sequences:
        desc: str = sequence.description()
        print(f'🔵 write \'{desc}.\' sequence')
        write_sequence(desc, sequence.seq())
        write_sequence_html(sequence.statistics().to_string(), desc, sequence.seq())


def parse_and_write_sequences(fasta_file_path: str) -> tuple[Sequence]:
    seq_records: list[SeqRecord] = parse(fasta_file_path)
    sequences = tuple([Sequence(keep_only_alphanum(seq_record.description), seq_record.seq, SequenceStatistics(seq_record.seq)) for seq_record in seq_records])
    write_sequences(sequences)
    return sequences


class Args:
    def __init__(self, fasta_files_path: tuple[str], error_message: str):
        self.__fasta_files_path = fasta_files_path
        self.__error_message = error_message

    def error(self) -> bool:
        return len(self.__error_message) > 0

    def error_message(self) -> str:
        return self.__error_message

    def fasta_files_path(self) -> tuple[str]:
        return self.__fasta_files_path

    @staticmethod
    def __with_files_path(fasta_files_path: tuple[str]) -> 'Args':
        return Args(fasta_files_path, '')

    @staticmethod
    def __error(error_message: str) -> 'Args':
        return Args(tuple(), error_message)

    @staticmethod
    def parse_args(args: list[str]) -> 'Args':
        if len(args) < 2:
            return Args.__error('❌ [No Argument] please provide fasta file or directory PATH as first argument')
        if len(args) > 2:
            return Args.__error('❌ [Too Many Argument] please provide fasta file or directory PATH as SOLE first argument')

        file_or_dir_path = args[1]

        if not os.path.exists(file_or_dir_path):
            return Args.__error(f'❌ [File Or Directory Not Found] \'{file_or_dir_path}\' not found')

        if os.path.isfile(file_or_dir_path):
            file_as_tuple = tuple([file_or_dir_path])
            return Args.__with_files_path(file_as_tuple)

        if os.path.isdir(file_or_dir_path):
            only_files = []
            for f in os.listdir(file_or_dir_path):
                f_path = os.path.join(file_or_dir_path, f)
                if os.path.isfile(f_path) and not f.startswith('.'):
                    only_files.append(f_path)

            if len(only_files) == 0:
                return Args.__error(f'❌ [No Files found] in directory \'{file_or_dir_path}\'')

            return Args.__with_files_path(tuple(only_files))


def header() -> str:
    now = datetime.now()
    formatted_date = now.strftime("%d/%m/%Y %H:%M:%S")
    return f'bio_parser - {VERSION} - {formatted_date}\n\n'


# TODO not tested
def write_summary(sequences: tuple[tuple[Sequence]]):
    Path('txt').mkdir(exist_ok=True)
    f = open(f'txt/summary.csv', 'w')
    f.write(header())
    for sequence in tuple(chain.from_iterable(sequences)):
        f.write(f'{sequence.statistics().significance().rounded()};{sequence.description()}\n')
    f.close()


if __name__ == '__main__':

    args: Args = Args.parse_args(sys.argv)
    if args.error():
        print(args.error_message())
        exit(-1)

    all_sequences: list[tuple[Sequence]] = []
    for fasta_file in args.fasta_files_path():
        sequences: tuple[Sequence] = parse_and_write_sequences(fasta_file)
        all_sequences.append(sequences)

    write_summary(all_sequences)
